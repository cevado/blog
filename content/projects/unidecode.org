#+title: Unidecode
#+date: 2020-06-05
#+author: cevado
#+type: projects
#+summary: elixir implementation of a unidecode library.
#+weight: 1
#+url: /projects/unidecode
#+draft: true
#+markup: org

An Unidecode elixir implementation. Unidecode is a common library in several languages, initially created in ~Perl~ that aims to transliterate utf8 strings to us-ascii strings. In elixir it can be useful to safely create charlists from utf8 strings.

** About
This one was created because of a past need when working with some operating systems stuff. At the time I needed it I found this ~unidecode~ library that solved the issue. I wanted to have something like that in elixir later on, so I started to build it. When I started it I was trying to get it automated in some way, so it would be updated as the UTF8 specs updated. I failed miserable on that idea. Right now I'm thinking to rewrite it and use the same idea as the original one in ~Perl~

** Status
 Stoped. It needs some maintainance and updates. But it can be useful in it's current state for someone with very basic needs. My idea is to maintain the same library api so when I start rewriting it, it doesn't break for people already using it.

** Links
+ [[https://github.com/fcevado/unidecode][Github]]
+ [[https://hex.pm/packages/unidecode][Hex]]
+ [[https://hexdocs.pm/unidecode/][Docs]]
